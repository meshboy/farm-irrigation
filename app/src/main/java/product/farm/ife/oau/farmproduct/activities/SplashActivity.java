package product.farm.ife.oau.farmproduct.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import product.farm.ife.oau.farmproduct.R;
import product.farm.ife.oau.farmproduct.utils.Constant;

public class SplashActivity extends AppCompatActivity {
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                SharedPreferences sharedPreferences = getSharedPreferences(Constant.PREFERENCE_KEY, 0);
                boolean checkMainActivity = sharedPreferences.getBoolean(Constant.SKIP_LOGIN_PAGE, false);

                if (checkMainActivity) {
                    intent = new Intent(SplashActivity.this, MainActivity.class);
                } else {
                    intent = new Intent(SplashActivity.this, LoginRegistrationActivity.class);
                }

                try {

                    Thread.sleep(3 * 1000);

                    startActivity(intent);
                    finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                    finish();
                }
            }
        });

        thread.start();
    }
}
