package product.farm.ife.oau.farmproduct.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import product.farm.ife.oau.farmproduct.R;
import product.farm.ife.oau.farmproduct.adapter.IrrigationHistoryAdapter;
import product.farm.ife.oau.farmproduct.adapter.SensorAdapter;
import product.farm.ife.oau.farmproduct.model.Farm;
import product.farm.ife.oau.farmproduct.utils.Constant;
import product.farm.ife.oau.farmproduct.utils.JsonParser;

/**
 * A simple {@link Fragment} subclass.
 */
public class IrrigationHistoryFragment extends Fragment {

    private SharedPreferences sharedPreferences;
    private String farmId;
    private ProgressBar progressBar;
    private List<Farm> farmList;
    private IrrigationHistoryAdapter irrigationHistoryAdapter;
    private ListView listView;

    public IrrigationHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        getActivity().setTitle(getString(R.string.view_irrigation_history));

        View view = inflater.inflate(R.layout.fragment_irrigation_history, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

        sharedPreferences = getActivity().getSharedPreferences(Constant.PREFERENCE_KEY, 0);
        farmId = sharedPreferences.getString(Constant.FARM_ID, "");

        listView = (ListView) view.findViewById(R.id.list_item);

        getIrrigationHistory();

        return view;
    }

    private void getIrrigationHistory (){
        showProgress(true);

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest strReq = new StringRequest(Request.Method.GET,
                Constant.FARM_HISTORY_URL + farmId, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                if (response != null) {

                    Log.d("ok", response);

                    showProgress(false);

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(Constant.SENSOR_HISTORY, response);
                    editor.apply();

                    farmList = JsonParser.parseIrrigationHistory(response);

                    irrigationHistoryAdapter = new IrrigationHistoryAdapter(getActivity(), android.R.layout.simple_list_item_1, farmList);
                    listView.setAdapter(irrigationHistoryAdapter);

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);

                Toasty.error(getActivity(), "Oops:) Something went wrong. Please check your internet connection", Toast.LENGTH_SHORT).show();

                farmList = JsonParser.parseSensorLog(sharedPreferences.getString(Constant.SENSOR_HISTORY, ""));

                irrigationHistoryAdapter = new IrrigationHistoryAdapter(getActivity(), android.R.layout.simple_list_item_1, farmList);
                listView.setAdapter(irrigationHistoryAdapter);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                return headers;
            }
        };

        //Adding request to request queue
        queue.add(strReq);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_dashboard, menu);  // Use filter.xml from step 1
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.refresh) {
            getIrrigationHistory();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showProgress(boolean isShowing){
        if(isShowing){
            progressBar.setVisibility(View.VISIBLE);
        }
        else{
            progressBar.setVisibility(View.GONE);
        }
    }

}
