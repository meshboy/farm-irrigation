package product.farm.ife.oau.farmproduct.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import product.farm.ife.oau.farmproduct.R;
import product.farm.ife.oau.farmproduct.activities.MainActivity;
import product.farm.ife.oau.farmproduct.model.Farm;
import product.farm.ife.oau.farmproduct.utils.Constant;
import product.farm.ife.oau.farmproduct.utils.JsonParser;
import product.farm.ife.oau.farmproduct.utils.NoSSLv3Factory;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private String[] soilTypeArray = {"Select Soil Type", "Clay Soil", "Sandy Soil", "Silt Soil"},
            irrigationArray = {"Select Irrigation Method", "Surface", "Sprinkler", "Drip/Trickle", "SubSurface"};

    private String soilType = "", irrigation = "";
    private String farmName, password, cropPlanted;
    private EditText farmNameText, passwordText, cropPlantedText;
    private ProgressDialog progressDialog;

    public RegistrationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_registration, container, false);

        getActivity().setTitle(R.string.create_account);

        farmNameText = (EditText) view.findViewById(R.id.farm_name);
        passwordText = (EditText) view.findViewById(R.id.password);
        cropPlantedText = (EditText) view.findViewById(R.id.crop_planted);

        ArrayAdapter<String> soilTypeArrayAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, soilTypeArray);
        soilTypeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner soilTypeSpinner = (Spinner) view.findViewById(R.id.soilTypeSpinner);
        soilTypeSpinner.setAdapter(soilTypeArrayAdapter);

        ArrayAdapter<String> irrigationArrayAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, irrigationArray);
        soilTypeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        soilTypeSpinner.setOnItemSelectedListener(this);

        Spinner irrigationSpinner = (Spinner) view.findViewById(R.id.irrigationSpinner);
        irrigationSpinner.setAdapter(irrigationArrayAdapter);
        irrigationSpinner.setOnItemSelectedListener(this);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.account_creation));
        progressDialog.setMessage(getString(R.string.setting_up));

        soilType = soilTypeArray[0];
        irrigation = irrigationArray[0];

        TextView loginTextView = (TextView) view.findViewById(R.id.login);
        loginTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, new LoginFragment()).addToBackStack(null).commit();
            }
        });

        view.findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest();
            }
        });


        return view;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (parent.getId()) {

            case 0:
                soilType = soilTypeArray[position];
                break;

            case 1:
                irrigation = irrigationArray[position];
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void sendRequest() {


        farmName = farmNameText.getText().toString();
        password = passwordText.getText().toString();
        cropPlanted = cropPlantedText.getText().toString();

        if (TextUtils.isEmpty(farmName)) {
            Toasty.info(getActivity(), "Enter Farm Name", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(password)) {
            Toasty.info(getActivity(), "Enter password", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(cropPlanted)) {
            Toasty.info(getActivity(), "Enter Crop Planted", Toast.LENGTH_SHORT).show();
        } else {
            progressDialog.show();

            HttpsURLConnection.setDefaultSSLSocketFactory(new NoSSLv3Factory());

            RequestQueue queue = Volley.newRequestQueue(getActivity());

            StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.REGISTRATION_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            progressDialog.hide();


                            if (response != null) {

                                Farm farm = JsonParser.parseLoginRegistration(response);

                                if (JsonParser.isStatus(response)) {

                                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constant.PREFERENCE_KEY, 0);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString(Constant.FARM_ID, farm.getFarmId());
                                    editor.putString(Constant.FARM_NAME, farmName);
                                    editor.putBoolean(Constant.SKIP_LOGIN_PAGE, true);
                                    editor.apply();

                                    Toasty.success(getActivity(), farm.getFeedback(), Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                } else {
                                    Toasty.error(getActivity(), farm.getFeedback(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.hide();

                    Toasty.error(getActivity(), "Oops:) something went wrong, please try again", Toast.LENGTH_SHORT).show();

                }
            }

            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put(Constant.PASSWORD_PARAM, password);
                    params.put(Constant.NAME_PARAM, farmName);
                    params.put(Constant.SOIL_TYPE_PARAM, soilType);
                    params.put(Constant.CROP_PLANTED, cropPlanted);
                    params.put(Constant.IRRIGATION_METHOD, irrigation);

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    return headers;
                }
            };

            int socketTimeout = 60000;//60 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postRequest.setRetryPolicy(policy);


            queue.add(postRequest);
        }
    }
}
