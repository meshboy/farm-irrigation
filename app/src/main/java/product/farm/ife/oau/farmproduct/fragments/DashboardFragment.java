package product.farm.ife.oau.farmproduct.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import product.farm.ife.oau.farmproduct.R;
import product.farm.ife.oau.farmproduct.activities.MainActivity;
import product.farm.ife.oau.farmproduct.model.Farm;
import product.farm.ife.oau.farmproduct.utils.Constant;
import product.farm.ife.oau.farmproduct.utils.JsonParser;
import product.farm.ife.oau.farmproduct.utils.NoSSLv3Factory;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {

    private ProgressBar progressBar;
    private String farmId, irrigationMode = "";
    private ProgressDialog progressDialog;
    private ToggleButton pumpStateButton;
    private int intPumpStatus = 0;
    private boolean pumpStatus = false;
    private EditText   soilMoistureText, soilTemperatureText, cropPlantedText, soilTypeText, messageText,
            airHumidityText, airTempText;


    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constant.PREFERENCE_KEY, 0);
        farmId = sharedPreferences.getString(Constant.FARM_ID, "");

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Pump Status");
        progressDialog.setMessage("setting pump status...");


        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

        getActivity().setTitle(getString(R.string.dashboard));

        soilMoistureText = (EditText) view.findViewById(R.id.soil_moisture);
        soilMoistureText.setEnabled(false);

        soilTemperatureText = (EditText) view.findViewById(R.id.soil_temperature);
        soilTemperatureText.setEnabled(false);

        cropPlantedText = (EditText) view.findViewById(R.id.crop_planted);
        cropPlantedText.setEnabled(false);

        soilTypeText = (EditText) view.findViewById(R.id.soil_type);
        soilTypeText.setEnabled(false);

        messageText = (EditText) view.findViewById(R.id.message);
        messageText.setEnabled(false);

        airHumidityText = (EditText) view.findViewById(R.id.air_humidity);
        airHumidityText.setEnabled(false);

        airTempText = (EditText) view.findViewById(R.id.air_temp);
        airTempText.setEnabled(false);

        pumpStateButton = (ToggleButton) view.findViewById(R.id.pump_state);


        pumpStateButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){
                    intPumpStatus = 1;
                }
                else {
                    intPumpStatus = 0;
                }

                setPumpStatus();
            }
        });

        getFarmDetails();


        return view;
    }

    private void getFarmDetails (){

        showProgress(true);

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest strReq = new StringRequest(Request.Method.GET,
                Constant.FARM_DETAILS_URL + farmId, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                if (response != null) {

                    Log.d("ok-dash", response);

                    showProgress(false);

                    processJsonResponse(response);

                  try {
                      SharedPreferences preferences = getActivity().getSharedPreferences(Constant.PREFERENCE_KEY, 0);
                      SharedPreferences.Editor editor = preferences.edit();
                      editor.putString(Constant.FARM_RESPONSE, response);
                      editor.apply();


                  }
                  catch (Exception ex){

                  }

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);

                SharedPreferences preferences = getActivity().getSharedPreferences(Constant.PREFERENCE_KEY, 0);
                processJsonResponse(preferences.getString(Constant.FARM_RESPONSE, ""));

                Toasty.error(getActivity(), "Oops:) Something went wrong. Please check your internet connection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                return headers;
            }
        };

        //Adding request to request queue
        queue.add(strReq);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_dashboard, menu);  // Use filter.xml from step 1
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.refresh) {
            getFarmDetails();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showProgress(boolean isShowing){
        if(isShowing){
            progressBar.setVisibility(View.VISIBLE);
        }
        else{
            progressBar.setVisibility(View.GONE);
        }
    }

    private void setPumpStatus (){
        progressDialog.show();

        HttpsURLConnection.setDefaultSSLSocketFactory(new NoSSLv3Factory());

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.SET_PUMP_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.hide();

                        if (response != null) {

                            if (JsonParser.isStatus(response)) {

                                pumpStatus = intPumpStatus == 1;

                                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constant.PREFERENCE_KEY, 0);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean(Constant.PUMP_STATUS,  pumpStatus);

                                pumpStateButton.setChecked(pumpStatus);

                                editor.apply();

                                Toasty.success(getActivity(),  JsonParser.getFeedback(response), Toast.LENGTH_SHORT).show();

                            } else {
                                Toasty.error(getActivity(),  JsonParser.getFeedback(response), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.hide();

                Toasty.error(getActivity(), "Oops:) something went wrong, please try again", Toast.LENGTH_SHORT).show();

            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(Constant.PUMP_STATE, String.valueOf(intPumpStatus));
                params.put(Constant.IRRIGATION_MODE, irrigationMode);
                params.put(Constant.FARM_ID_PARAM, farmId);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                return headers;
            }
        };

        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);


        queue.add(postRequest);
    }

    private void processJsonResponse(String response){

        Farm farm = JsonParser.getFarmDetails(response);

        Log.d("ok--", farm.toString());

        if(farm != null){

            soilMoistureText.setText(farm.getSoilMoisture());
            soilTemperatureText.setText(farm.getAirTemperature());
            cropPlantedText.setText(farm.getCropPlanted());
            soilTypeText.setText(farm.getSoilType());
            messageText.setText(farm.getMessage());
            airHumidityText.setText(farm.getAirHumidity());
            airTempText.setText(farm.getAirTemperature());

            irrigationMode = farm.getStringIrrigationMode();

            pumpStatus = farm.isPumpStatus();

            if(pumpStatus){

                pumpStateButton.setChecked(true);

            }
            else{
                pumpStateButton.setChecked(false);
            }
        }
        else{
            Log.d("ok--", "yeepa");
        }
    }

}
