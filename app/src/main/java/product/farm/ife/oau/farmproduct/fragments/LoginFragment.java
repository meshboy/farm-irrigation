package product.farm.ife.oau.farmproduct.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import product.farm.ife.oau.farmproduct.R;
import product.farm.ife.oau.farmproduct.activities.MainActivity;
import product.farm.ife.oau.farmproduct.model.Farm;
import product.farm.ife.oau.farmproduct.utils.Constant;
import product.farm.ife.oau.farmproduct.utils.JsonParser;
import product.farm.ife.oau.farmproduct.utils.NoSSLv3Factory;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    private String farmName, password;
    private EditText farmNameText, passwordText;
    private ProgressDialog progressDialog;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        getActivity().setTitle(R.string.login);

        farmNameText = (EditText) view.findViewById(R.id.farm_name);
        passwordText = (EditText) view.findViewById(R.id.password);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.login));
        progressDialog.setMessage(getString(R.string.verify_user));

        TextView createAccountTExtView = (TextView) view.findViewById(R.id.create_account);
        createAccountTExtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, new RegistrationFragment()).addToBackStack(null).commit();
            }
        });

        view.findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loginRequest();
            }
        });

        return view;
    }


    private void loginRequest() {

        farmName = farmNameText.getText().toString();
        password = passwordText.getText().toString();

        if (TextUtils.isEmpty(farmName)) {
            Toasty.info(getActivity(), "Enter farm Name", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(password)) {
            Toasty.info(getActivity(), "Enter password", Toast.LENGTH_SHORT).show();
        } else {

            progressDialog.show();

            HttpsURLConnection.setDefaultSSLSocketFactory(new NoSSLv3Factory());

            RequestQueue queue = Volley.newRequestQueue(getActivity());

            StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.LOGIN_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {


                            progressDialog.hide();

                            if (response != null) {

                                Log.d("ok", response);

                                Farm farm = JsonParser.parseLoginRegistration(response);

                                if (JsonParser.isStatus(response)) {

                                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constant.PREFERENCE_KEY, 0);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString(Constant.FARM_ID, farm.getFarmId());

                                    Log.d("ok-farm", farm.getFarmId());

                                    editor.putString(Constant.FARM_NAME, farmName);
                                    editor.putBoolean(Constant.SKIP_LOGIN_PAGE, true);
                                    editor.apply();

                                    Toasty.success(getActivity(), farm.getFeedback(), Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                } else {
                                    Toasty.error(getActivity(), farm.getFeedback(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.hide();

                    Toasty.error(getActivity(), "Oops:) something went wrong, please try again", Toast.LENGTH_SHORT).show();

                }
            }

            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put(Constant.PASSWORD_PARAM, password);
                    params.put(Constant.NAME_PARAM, farmName);

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    return headers;
                }
            };

            int socketTimeout = 60000;//60 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postRequest.setRetryPolicy(policy);


            queue.add(postRequest);
        }
    }

}
