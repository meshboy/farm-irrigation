package product.farm.ife.oau.farmproduct.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import product.farm.ife.oau.farmproduct.model.Farm;

/**
 * Created by meshboy on 4/3/17.
 */

public class JsonParser {

    public static Farm parseLoginRegistration(String jsonData) {

        Farm farm = new Farm();

        try {
            JSONObject jsonObject = new JSONObject(jsonData);

            JSONObject dataObject = jsonObject.getJSONObject("data");

            String farmId = dataObject.getString("farm_id"),
                    feedback = dataObject.getString("feedback");

            farm.setFarmId(farmId);
            farm.setFeedback(feedback);

            return farm;


        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }
    }

    public static String getFeedback(String jsonData) {
        try {
            JSONObject jsonObject = new JSONObject(jsonData);

            JSONObject dataObject = jsonObject.getJSONObject("data");

            return dataObject.getString("feedback");

        } catch (JSONException e) {
            e.printStackTrace();

            return "";
        }
    }

    public static boolean isStatus(String jsonData) {

        try {
            JSONObject jsonObject = new JSONObject(jsonData);

            return jsonObject.getBoolean("status");

        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static Farm getFarmDetails (String jsonData){

        Farm farm = new Farm();
        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            JSONObject dataObject = jsonObject.getJSONObject("data");

            JSONObject detailsObject = dataObject.getJSONObject("Dashboard details");

            farm.setSoilMoisture(detailsObject.getString("soil_moisture"));
            farm.setSoilTemperature(detailsObject.getString("soil_temperature"));
            farm.setAirHumidity(detailsObject.getString("weather_humidity"));
            farm.setAirTemperature(detailsObject.getString("weather_temperature"));
            farm.setPumpStatus(detailsObject.getInt("pump_state") == 1);
            farm.setCropPlanted(detailsObject.getString("crop_planted"));
            farm.setSoilType(detailsObject.getString("soil_type"));
            farm.setStringIrrigationMode(detailsObject.getString("irrigation mode"));

            farm.setMessage(detailsObject.getString("message"));

            return farm;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Farm> parseSensorLog (String jsonData){

        List<Farm> farmList = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            JSONObject dataObject = jsonObject.getJSONObject("data");
            JSONArray sensorArray = dataObject.getJSONArray("Log details");

            for(int i = 0; i < sensorArray.length(); i++){

                JSONObject object = sensorArray.getJSONObject(i);

                Farm farm = new Farm();

                farm.setSoilMoisture(object.getString("soil_moisture"));
                farm.setSoilTemperature(object.getString("soil_temperature"));
                farm.setAirTemperature(object.getString("weather_temperature"));
                farm.setAirHumidity(object.getString("weather_humidity"));
                farm.setPumpStatus(object.getInt("pump_state") == 1);
                farm.setDateCreated(object.getString("date_created"));

                farmList.add(farm);
            }

            return farmList;

        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }
    }

    public static List<Farm> parseIrrigationHistory(String jsonData){
        List<Farm> farmList = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            JSONObject dataObject = jsonObject.getJSONObject("data");
            JSONArray historyArray = dataObject.getJSONArray("History details");

            for(int i = 0; i < historyArray.length(); i++){

                JSONObject object = historyArray.getJSONObject(i);

                Farm farm = new Farm();

                farm.setSoilMoisture(object.getString("soil_moisture"));
                farm.setSoilTemperature(object.getString("soil_temperature"));
                farm.setAirTemperature(object.getString("weather_temperature"));
                farm.setAirHumidity(object.getString("weather_humidity"));
                farm.setPumpStatus(object.getString("pump_state").equals("1"));
                farm.setDateCreated(object.getString("date_created"));
                farm.setHistoryStartDate(object.getString("history_start_date"));
                farm.setStringIrrigationMode(object.getString("irrigation_mode"));
                farm.setGallonUsed(object.getString("gallon_water_used"));
                farm.setPumpStatusTime(object.getString("pump_status_time"));



                farmList.add(farm);
            }

            return farmList;

        } catch (JSONException e) {
            e.printStackTrace();

            return null;
        }
    }
}
