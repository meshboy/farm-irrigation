package product.farm.ife.oau.farmproduct.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import product.farm.ife.oau.farmproduct.R;
import product.farm.ife.oau.farmproduct.model.Farm;

/**
 * Created by meshboy on 4/18/17.
 */

public class SensorAdapter extends ArrayAdapter<Farm> {

    private List<Farm> farmList;
    private Context context;

    public SensorAdapter(Context context, int resource, List<Farm> farmList) {
        super(context, resource, farmList);

        this.context = context;
        this.farmList = farmList;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        view = layoutInflater.inflate(R.layout.list_row, parent, false);

        TextView soilMoistureTextView = (TextView) view.findViewById(R.id.soil_moisture),
                weatherHumidityTextView = (TextView) view.findViewById(R.id.weather_humidity),
                soilTemperatureTextView = (TextView) view.findViewById(R.id.soil_temperature),
                pumpStatusText = (TextView) view.findViewById(R.id.pump_state),
                weatherTemperatureTextView = (TextView) view.findViewById(R.id.weather_temperature),
                dateCreatedTextView = (TextView) view.findViewById(R.id.date_created);

        soilMoistureTextView.setText(farmList.get(position).getSoilMoisture());
        weatherHumidityTextView.setText(farmList.get(position).getAirHumidity());
        soilTemperatureTextView.setText(farmList.get(position).getSoilTemperature());
        pumpStatusText.setText(farmList.get(position).isPumpStatus() ? "ON" : "OFF");
        weatherTemperatureTextView.setText(farmList.get(position).getAirTemperature());
        dateCreatedTextView.setText(farmList.get(position).getDateCreated());

        return view;
    }
}
