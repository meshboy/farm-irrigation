package product.farm.ife.oau.farmproduct.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import product.farm.ife.oau.farmproduct.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UsageInfoFragment extends Fragment {


    public UsageInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle(getString(R.string.usage_info));
        return inflater.inflate(R.layout.fragment_usage_info, container, false);
    }

}
