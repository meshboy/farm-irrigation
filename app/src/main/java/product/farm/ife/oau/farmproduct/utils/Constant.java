package product.farm.ife.oau.farmproduct.utils;

/**
 * Created by meshboy on 3/16/17.
 */

public class Constant {

    private static final String BASE_URL = "https://farmirrigation.herokuapp.com/";

    public static final String PREFERENCE_KEY = "peferenceKey";

    public static final String SKIP_LOGIN_PAGE = "skipLoginPage";

    public static final String LOGIN_URL = BASE_URL + "login";
    public static final String REGISTRATION_URL = BASE_URL + "register";
    public static final String FARM_DETAILS_URL = BASE_URL + "dashboard/";
    public static final String IRRIGATION_URL = BASE_URL + "set_mode";
    public static String SENSOR_LOGS_URL = BASE_URL + "sensor_logs/";
    public static String FARM_HISTORY_URL = BASE_URL + "irrigation_history/";
    public static String SET_PUMP_URL = BASE_URL + "set_pump";

    public static final String PASSWORD_PARAM = "password";
    public static final String NAME_PARAM = "farm_name";
    public static final String SOIL_TYPE_PARAM = "soil_type";
    public static final String CROP_PLANTED = "crop_planted";
    public static final String IRRIGATION_METHOD = "irrigation_method";
    public static final String FARM_ID = "farmId";
    public static final String FARM_NAME = "farmName";
    public static final String SOIL_MOISTURE = "soilMoisture";
    public static final String SOIL_TEMPERATURE = "soilTemperature";
    public static final String AIR_HUMIDITY = "airHumidity";
    public static final String AIR_TEMPERATURE = "airTemperature";
    public static final String PUMP_STATUS = "pumpStatus";
    public static final String MESSAGE = "message";
    public static final String SOIL_TYPE = "soilType";
    public static final String SENSOR_HISTORY = "sensorHistory";
    public static final String IRRIGATION_MODE_ = "irrigationString";
    public static final String FARM_RESPONSE = "response";

    public static final String IRRIGATION_MODE = "irrigation_mode";
    public static final String MIN_SOIL = "min_soil_moisture";
    public static final String MAX_SOIL = "max_soil_moisture";
    public static final String MAX_SOIL_TEMPERATURE = "max_soil_temp";
    public static final String NOTIFICATION_INT = "notification_interval";
    public static final String PUMP_STATE = "pump_state";
    public static final String FARM_ID_PARAM = "farm_id";
}
