package product.farm.ife.oau.farmproduct.model;

/**
 * Created by meshboy on 4/4/17.
 */

public class Farm {

    private String farmId;
    private String stringIrrigationMode;
    private String soilMoisture;
    private String soilTemperature;
    private String farmName;
    private String dateCreated;
    private String feedback;
    private String cropPlanted;
    private String soilType;
    private String message;
    private String airHumidity;
    private String airTemperature;
    private String pumpStatusTime;
    private String historyStartDate;
    private String gallonUsed;
    private int irrigationMode;
    private boolean pumpStatus;

    public String getStringIrrigationMode() {
        return stringIrrigationMode;
    }

    public void setStringIrrigationMode(String stringIrrigationMode) {
        this.stringIrrigationMode = stringIrrigationMode;
    }

    public String getGallonUsed() {
        return gallonUsed;
    }

    public void setGallonUsed(String gallonUsed) {
        this.gallonUsed = gallonUsed;
    }

    public int getIrrigationMode() {
        return irrigationMode;
    }

    public void setIrrigationMode(int irrigationMode) {
        this.irrigationMode = irrigationMode;
    }

    public String getPumpStatusTime() {
        return pumpStatusTime;
    }

    public void setPumpStatusTime(String pumpStatusTime) {
        this.pumpStatusTime = pumpStatusTime;
    }

    public String getHistoryStartDate() {
        return historyStartDate;
    }

    public void setHistoryStartDate(String historyStartDate) {
        this.historyStartDate = historyStartDate;
    }

    public String getSoilType() {
        return soilType;
    }

    public void setSoilType(String soilType) {
        this.soilType = soilType;
    }

    public String getCropPlanted() {
        return cropPlanted;
    }

    public void setCropPlanted(String cropPlanted) {
        this.cropPlanted = cropPlanted;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAirHumidity() {
        return airHumidity;
    }

    public void setAirHumidity(String airHumidity) {
        this.airHumidity = airHumidity;
    }

    public String getAirTemperature() {
        return airTemperature;
    }

    public void setAirTemperature(String airTemperature) {
        this.airTemperature = airTemperature;
    }

    public boolean isPumpStatus() {
        return pumpStatus;
    }

    public void setPumpStatus(boolean pumpStatus) {
        this.pumpStatus = pumpStatus;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }


    public String getFarmId() {
        return farmId;
    }

    public void setFarmId(String farmId) {
        this.farmId = farmId;
    }

    public String getSoilMoisture() {
        return soilMoisture;
    }

    public void setSoilMoisture(String soilMoisture) {
        this.soilMoisture = soilMoisture;
    }

    public String getSoilTemperature() {
        return soilTemperature;
    }

    public void setSoilTemperature(String soilTemperature) {
        this.soilTemperature = soilTemperature;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getFarmName() {
        return farmName;
    }

    public void setFarmName(String farmName) {
        this.farmName = farmName;
    }
}
