package product.farm.ife.oau.farmproduct.fragments;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import io.apptik.widget.MultiSlider;
import product.farm.ife.oau.farmproduct.R;
import product.farm.ife.oau.farmproduct.utils.Constant;
import product.farm.ife.oau.farmproduct.utils.JsonParser;
import product.farm.ife.oau.farmproduct.utils.NoSSLv3Factory;

/**
 * A simple {@link Fragment} subclass.
 */
public class IrrigationModeFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private String[] irrigationArray = {"Automatic", "Controlled"};
    private int irrigationMode;
    private LinearLayout automaticLinearLayout, controlledLinearLayout;
    private EditText minSoilEditText, maxSoilEditText, maxSoilTempEditText;
    private String pumpState = "", notificationInterval = "", minSoil, maxSoil, maxTemp, farmId;
    private ProgressDialog progressDialog;
    private TextView intervalTextView;

    public IrrigationModeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle(getString(R.string.set_irrigation));

        View view = inflater.inflate(R.layout.fragment_irrigation_mode, container, false);

        intervalTextView = (TextView) view.findViewById(R.id.interval_level);
        intervalTextView.setText(getString(R.string.level).concat(" 0"));

        irrigationMode = 1;

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.set_irrigation_mode));

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constant.PREFERENCE_KEY, 0);
        farmId = sharedPreferences.getString(Constant.FARM_ID, "");

        AppCompatSpinner appCompatSpinner = (AppCompatSpinner) view.findViewById(R.id.irrigationSpinner);
        ArrayAdapter<String> irrigationAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, irrigationArray);
        irrigationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        appCompatSpinner.setAdapter(irrigationAdapter);
        appCompatSpinner.setOnItemSelectedListener(this);

        automaticLinearLayout = (LinearLayout) view.findViewById(R.id.automatic);
        controlledLinearLayout = (LinearLayout) view.findViewById(R.id.controlled);

        minSoilEditText = (EditText) view.findViewById(R.id.min_soil_moisture);
        maxSoilEditText = (EditText) view.findViewById(R.id.max_soil_moisture);
        maxSoilTempEditText = (EditText) view.findViewById(R.id.max_soil_temp);

        ToggleButton pumpToggleButton = (ToggleButton) view.findViewById(R.id.pump_state);
        pumpToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    pumpState = "1";
                } else {
                    pumpState = "0";
                }
            }
        });

        MultiSlider multiSlider5 = (MultiSlider) view.findViewById(R.id.slider);

        multiSlider5.setOnThumbValueChangeListener(new MultiSlider.OnThumbValueChangeListener() {
            @Override
            public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
                notificationInterval = String.valueOf(value);

                intervalTextView.setText(getString(R.string.level).concat(" ").concat(String.valueOf(value)));
            }
        });

        view.findViewById(R.id.set_irrigation_mode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (irrigationMode != 0) {
                    getValues();
                } else {

                    sendRequest();
                }
            }
        });

        return view;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (position != 0) {
            irrigationMode = 0; //controlled mode
            automaticLinearLayout.setVisibility(View.GONE);
            controlledLinearLayout.setVisibility(View.VISIBLE);
        } else {
            irrigationMode = 1; //automatic mode
            automaticLinearLayout.setVisibility(View.VISIBLE);
            controlledLinearLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void getValues() {

        minSoil = minSoilEditText.getText().toString();
        maxSoil = maxSoilEditText.getText().toString();
        maxTemp = maxSoilTempEditText.getText().toString();

        if (TextUtils.isEmpty(minSoil)) {
            Toasty.info(getActivity(), "Enter minimum soil", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(maxSoil)) {
            Toasty.info(getActivity(), "Enter maximum soil", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(maxTemp)) {
            Toasty.info(getActivity(), "Enter maximum temperature", Toast.LENGTH_SHORT).show();
        } else {
            sendRequest();
        }
    }

    private void sendRequest() {

        if (irrigationMode != 0) {
            progressDialog.setMessage(getString(R.string.automatic));
        } else {
            progressDialog.setMessage(getString(R.string.controlled));
        }
        progressDialog.show();

        HttpsURLConnection.setDefaultSSLSocketFactory(new NoSSLv3Factory());

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest postRequest = new StringRequest(Request.Method.POST, Constant.IRRIGATION_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialog.hide();

                        if (response != null) {

                            boolean status = JsonParser.isStatus(response);

                            if (status) {
                                Toasty.success(getActivity(), JsonParser.getFeedback(response), Toast.LENGTH_SHORT).show();
                            } else {
                                Toasty.error(getActivity(), JsonParser.getFeedback(response), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.hide();

                Toasty.error(getActivity(), "Oops:) something went wrong, please try again", Toast.LENGTH_SHORT).show();

            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(Constant.IRRIGATION_MODE, String.valueOf(irrigationMode));
                params.put(Constant.FARM_ID_PARAM, farmId);
                if (irrigationMode != 0) {
                    params.put(Constant.MIN_SOIL, minSoil);
                    params.put(Constant.MAX_SOIL, maxSoil);
                    params.put(Constant.MAX_SOIL_TEMPERATURE, maxTemp);
                } else {
                    params.put(Constant.NOTIFICATION_INT, notificationInterval);
                    params.put(Constant.PUMP_STATE, pumpState);
                }


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                return headers;
            }
        };

        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);


        queue.add(postRequest);

    }


}
